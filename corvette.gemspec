# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'corvette/version'

Gem::Specification.new do |spec|
	spec.name = "corvette"
	spec.version = Corvette::VERSION
	spec.authors = ["BroderickBrockman"]
	spec.email = ["broderickbrockman@gmail.com"]
	spec.summary = %q{Kind of new rails architecture solution}
	spec.description = %q{Kind of new rails architecture solution}
	spec.homepage = ""
	spec.license = "MIT"

	spec.files = `git ls-files -z`.split("\x0")
	spec.executables = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
	spec.test_files = spec.files.grep(%r{^(test|spec|features)/})
	spec.require_paths = ["lib"]

	spec.add_development_dependency "bundler", "~> 1.6"
	spec.add_development_dependency "rake"
	spec.add_runtime_dependency "reform"
	spec.add_runtime_dependency "hashie"
	spec.add_runtime_dependency "activesupport"
	spec.add_runtime_dependency "virtus"
end
