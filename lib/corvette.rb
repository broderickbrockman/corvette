module Corvette
	module Helper
		def corvette_concept(name)
			"#{name}_concept".camelcase.constantize
		end
	end
end

require "corvette/version"
require "corvette/railtie"
require "corvette/operation"
require "corvette/procedure"
require "corvette/action"
require "corvette/service"
require "corvette/assertion"

