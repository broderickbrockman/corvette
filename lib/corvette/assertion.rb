module Corvette
	module Assertion
		def assert &block
			raise ArgumentError unless block.call()
		end
	end
end