require 'corvette/assertion'
require 'corvette/act'
module Corvette
	class Procedure < Act

		include Validation
		include Auxiliary
		include Required

		def procedure(params)
			procedure_log(:start)
			process_params(params)
			process_auxiliary
			process_required
			procedure_validate(params)
			yield
			act_success
			procedure_log(:end)
		rescue StandardError => exception
			log_error(exception, params)
			act_error(exception)
		end

		private

		def procedure_validate(params)
			model = OpenStruct.new
			contract = contract_klass.new(model)

			unless contract.validate(params)
				exception = ValidationError.new("validation failed")
				exception.errors = contract.errors.messages
				raise exception
			end
			procedure_log(:validation_success)
		end

		def procedure_log(kind)
			case kind
			when :start
				logger.info "procedure started (#{act_name})"
			when :end
				logger.info "procedure ended (#{act_name})"
			when :validation_success
				logger.info "params validation succeeded (#{act_name})"
			end
		end
	end
end