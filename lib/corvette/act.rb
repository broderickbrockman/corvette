require 'corvette/assertion'
module Corvette
	class Act
		class ActError < StandardError
			attr_accessor :act
		end

		include Assertion
		# include ActiveSupport::Rescuable
		# rescue_from ERRORS, with: :log_error

		def initialize(options={})
			@options = options
			process_callbacks
		end

		def run
			raise NotImplementedError, 'must be overridden'
		end

		def process_params(params)
			assert { params.is_a?(Hash) }

			if params.is_a?(Hashie::Mash)
				@params = params
			else
				@params = Hashie::Mash.new(params)
			end
		end

		def process_callbacks
			@error_callback = @options[:on_error]
			@success_callback = @options[:on_success]
		end

		def act_success
			@success_callback.call(self) if @success_callback.is_a?(Proc)
		end

		def act_error(exception)
			@error_callback.call(self, exception) if @error_callback.is_a?(Proc)
		end

		def log_error(exception, params = nil)
			message = "message: #{ exception.message }; place: #{act_name}; (#{ exception.class })"

			case exception
			when Validation::ValidationError, Required::RequiredError
				message << " error: #{exception.errors.inspect};"
			else
				#nothing
			end

			unless params.nil?
				message << " params: #{params.inspect}"
			end

			Rails.logger.error message
		end

		def logger
			Rails.logger
		end

		def act_name
			@act_name ||= self.class.name.to_s
		end

		module Auxiliary
			def process_auxiliary
				if @options[:auxiliary]
					@auxiliary = Hashie::Mash.new(@options[:auxiliary])
				else
					@auxiliary = Hashie::Mash.new
				end
				singleton_class.class_eval do
					attr_reader :auxiliary
				end
			end
		end

		module Required
			class RequiredError < StandardError
				attr_accessor :errors
			end

			def process_required
				if self.class.const_defined?(:RequiredContract)
					@required = Hashie::Mash.new(@options[:required]||{})
					required_contract = required_contract_class.new(@required)
					unless required_contract.validate
						exception = RequiredError.new
						exception.errors = required_contract.errors.messages
						raise exception
					end
					singleton_class.class_eval do
						attr_reader :required
					end
				end
			rescue StandardError => exception
				# rescue_with_handler(exception) || raise
				log_error(exception)
				act_error(exception)
			end

			def required_contract_class
				self.class.const_get :RequiredContract
			end
		end

		module Validation
			class ValidationError < StandardError
				attr_accessor :errors
			end

			def contract_klass
				self.class.const_get :Contract
			end
		end
	end
end
# rescue_with_handler(exception) || raise