require 'corvette/assertion'
require 'corvette/act'

module Corvette
	class Action < Act
		include Auxiliary
		include Required

		def action(params)
			process_params(params)
			yield
			act_success
		rescue StandardError => exception
			log_error(exception, params)
			act_error(exception)
		end
	end
end