module Corvette
	class Railtie < ::Rails::Railtie
		initializer 'corvette.basic', :after => :add_routing_paths do |app|
			module StringGenerator
				def generate_string(length)
					o = [('a'..'z'), ('A'..'Z'), (0..9)].map { |i| i.to_a }.flatten
					(0..length).map { o[rand(o.length)] }.join
				end
			end
			Object.include(StringGenerator)
		end
	end
end