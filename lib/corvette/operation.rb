# require 'corvette/operation/model'
# require 'corvette/operation/multiple_models'
# require 'corvette/operation/anonymous_model'
# require 'corvette/operation/background_job'
# require 'corvette/operation/active_record_model'

require 'corvette/assertion'
require 'corvette/act'

module Corvette
	class Operation < Act
		include Validation
		extend Forwardable

		delegate [:model, :errors] => :contract

		attr_reader :contract

		def operation(model, params)
			operation_log(:start)
			process_params(params)
			operation_validate(model, params)
			yield
			act_success
			operation_log(:end)
		rescue StandardError => exception
			log_error(exception, params)
			act_error(exception)
		end

		private

		def operation_validate(model, params)
			@contract = contract_klass.new(model)
			unless contract.validate(params)
				exception = ValidationError.new("validation failed")
				exception.errors = contract.errors.messages
				raise exception
			end
			operation_log(:validation_success)
		end

		def operation_log(kind)
			case kind
			when :start
				logger.info "operation started (#{act_name})"
			when :end
				logger.info "operation ended (#{act_name})"
			when :validation_success
				logger.info "params validation succeeded (#{act_name})"
			end
		end
	end
end
